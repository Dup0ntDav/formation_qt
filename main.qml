import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.13

Window {
    id: window
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    Button {
        id: buttonLogin
        width: textField.width
        enabled: textField.text.length !== 0

        text: qsTr("Login")
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
    }


    TextField {
        id: textField
        anchors.horizontalCenter: buttonLogin.horizontalCenter
        placeholderText: qsTr("Enter your name")
        anchors.bottom: buttonLogin.top
        anchors.bottomMargin: 10
    }


}
